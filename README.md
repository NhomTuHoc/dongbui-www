[[runners]]
  name = "runner"
  url = "https://gitlab.com/ci"
  token = "TOKEN"
  executor = "docker"
  [runners.docker]
    privileged = true
